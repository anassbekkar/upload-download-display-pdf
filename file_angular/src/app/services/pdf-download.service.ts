import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PdfDownloadService {
  constructor(private http: HttpClient) {}

  downloadPdf(fileName: string): Observable<Blob> {
    return this.http.get<Blob>(`https://localhost:8080/download-file/${fileName}`, { responseType: 'blob' as 'json' });
  }
}
