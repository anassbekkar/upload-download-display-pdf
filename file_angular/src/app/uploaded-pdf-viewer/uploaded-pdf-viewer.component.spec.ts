import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadedPdfViewerComponent } from './uploaded-pdf-viewer.component';

describe('UploadedPdfViewerComponent', () => {
  let component: UploadedPdfViewerComponent;
  let fixture: ComponentFixture<UploadedPdfViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UploadedPdfViewerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UploadedPdfViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
