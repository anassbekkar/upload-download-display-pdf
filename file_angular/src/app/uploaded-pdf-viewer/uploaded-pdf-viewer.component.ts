import { Component } from '@angular/core';

@Component({
  selector: 'app-uploaded-pdf-viewer',
  standalone: true,
  imports: [],
  templateUrl: './uploaded-pdf-viewer.component.html',
  styleUrl: './uploaded-pdf-viewer.component.css'
})
export class UploadedPdfViewerComponent {

}
