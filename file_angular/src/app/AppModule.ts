import { HttpClientModule } from '@angular/common/http';
import { NO_ERRORS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AppRoutingModule } from './app.routes';
import { FileListComponent } from './components/file-list/file-list.component';
import { FileUploadService } from './services/file-upload.service';
import { PdfDownloadService } from './services/pdf-download.service';
import { UploadFilesComponent } from './upload-files/upload-files.component';
import { UploadedPdfViewerComponent } from './uploaded-pdf-viewer/uploaded-pdf-viewer.component';


@NgModule({
  declarations: [
    UploadedPdfViewerComponent,
    FileListComponent,
    UploadFilesComponent,
    // Other components
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    PdfViewerModule,
    // Other modules
  ],
  providers: [
    FileUploadService,
    PdfDownloadService,
    // Other services
  ],
  schemas: [NO_ERRORS_SCHEMA,],
  bootstrap: [UploadFilesComponent] // Adjust if needed
})
export class AppModule {
}
