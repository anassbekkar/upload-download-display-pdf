import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileListComponent } from './components/file-list/file-list.component'; // Import FileListComponent
import { UploadedPdfViewerComponent } from './uploaded-pdf-viewer/uploaded-pdf-viewer.component';
 // Import PdfViewerComponent
import { UploadFilesComponent } from './upload-files/upload-files.component'; // Import UploadFilesComponent

// Define the routes
const routes: Routes = [
  { path: '', redirectTo: '/upload', pathMatch: 'full' }, // Redirect to the upload page by default
  { path: 'upload', component: UploadFilesComponent }, // Route for the file upload page
  { path: 'file-list', component: FileListComponent }, // Route for the file list page
  { path: 'pdf-viewer', component: UploadedPdfViewerComponent }, // Route for the PDF viewer page

  // Add other routes as needed
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// Export the routes variable
export { routes };

