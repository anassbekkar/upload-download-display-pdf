import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css']
})
export class FileListComponent implements OnInit {
  uploadedFiles: string[] = ['C:\Users\Dell\angularfile\file_angular\src\assets\doc_technique.pdf']; // Exemple de liste de fichiers téléversés

  constructor() { }

  ngOnInit(): void {
  }

}

